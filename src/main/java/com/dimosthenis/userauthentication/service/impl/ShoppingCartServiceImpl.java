package com.dimosthenis.userauthentication.service.impl;

import com.dimosthenis.userauthentication.entity.Product;
import com.dimosthenis.userauthentication.repository.ProductRepository;
import com.dimosthenis.userauthentication.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ProductRepository productRepository;

    private Map<Product, Integer> productList = new HashMap<>();


    @Override
    public void addProduct(Product product) {
        if (productList.containsKey(product)) {
            productList.replace(product, productList.get(product)+1);
        }
        else {
            productList.put(product, 1);
        }
    }

    @Override
    public void removeProduct(Product product) {
        if (productList.containsKey(product)) {
            if (productList.get(product) > 1) {
                productList.replace(product, productList.get(product) - 1);
            }
            else {
                productList.remove(product);
            }
        }
    }

    @Override
    public Map<Product, Integer> getAllProducts() {
        return Collections.unmodifiableMap(productList);
    }

    @Override
    public BigDecimal getTotal() {
        return productList.entrySet().stream()
                .map(entry -> entry.getKey().getPrice().multiply(BigDecimal.valueOf(entry.getValue())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }


}
