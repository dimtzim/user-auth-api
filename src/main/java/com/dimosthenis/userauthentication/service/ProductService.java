package com.dimosthenis.userauthentication.service;

import com.dimosthenis.userauthentication.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Optional<Product> findById(Long product_id);

    List<Product> getAllProducts();

    Product addProduct(Product product);
}
