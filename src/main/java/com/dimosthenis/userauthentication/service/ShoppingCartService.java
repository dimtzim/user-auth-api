package com.dimosthenis.userauthentication.service;

import com.dimosthenis.userauthentication.entity.Product;

import java.math.BigDecimal;
import java.util.Map;

public interface ShoppingCartService {

    void addProduct(Product product);

    void removeProduct(Product product);

    Map<Product, Integer> getAllProducts();

    BigDecimal getTotal();

}
