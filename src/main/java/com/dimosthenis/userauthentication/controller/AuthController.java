package com.dimosthenis.userauthentication.controller;

import com.dimosthenis.userauthentication.entity.ERole;
import com.dimosthenis.userauthentication.entity.Role;
import com.dimosthenis.userauthentication.entity.User;
import com.dimosthenis.userauthentication.payload.request.LoginRequest;
import com.dimosthenis.userauthentication.payload.request.RegisterRequest;
import com.dimosthenis.userauthentication.payload.response.JwtResponse;
import com.dimosthenis.userauthentication.payload.response.MessageResponse;
import com.dimosthenis.userauthentication.repository.RoleRepository;
import com.dimosthenis.userauthentication.repository.UserRepository;
import com.dimosthenis.userauthentication.security.jwt.JwtTokenProvider;
import com.dimosthenis.userauthentication.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        String username = loginRequest.getUsername();
        Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginRequest.getPassword()));


        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());


        String token = jwtTokenProvider.createToken(username, roles);


        return ResponseEntity.ok(new JwtResponse(token,
            userDetails.getId(),
            userDetails.getUsername(),
            userDetails.getEmail(),
            roles));


    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!", false));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!", false));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Role userRole = roleRepository.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("User Role not set"));
        user.setRoles(Collections.singleton(userRole));

        userRepository.save(user);
        return ok(new MessageResponse("User registered successfully!", true));
    }
}