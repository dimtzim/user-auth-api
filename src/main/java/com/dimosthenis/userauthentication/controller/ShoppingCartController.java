package com.dimosthenis.userauthentication.controller;

import com.dimosthenis.userauthentication.service.impl.ProductServiceImpl;
import com.dimosthenis.userauthentication.service.impl.ShoppingCartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shopping-cart")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartServiceImpl shoppingCartService;

    @Autowired
    private ProductServiceImpl productService;

    @GetMapping
    public ResponseEntity<?> getAllInCart() {
        return new ResponseEntity<>(shoppingCartService.getAllProducts(),HttpStatus.OK);
    }

    @GetMapping("/add-product/{productId}")
    public ResponseEntity<String> addProduct(@PathVariable("productId") Long productId) {
        productService.findById(productId).ifPresent(shoppingCartService::addProduct);
        return new ResponseEntity<>("Product added successfully.", HttpStatus.OK);
    }

    @GetMapping("/remove-product/{productId}")
    public ResponseEntity<String> removeProduct(@PathVariable("productId") Long productId) {
        productService.findById(productId).ifPresent(shoppingCartService::removeProduct);
        return new ResponseEntity<>("Product removed successfully.", HttpStatus.OK);
    }

    @GetMapping("/total-price")
    public ResponseEntity<?> getTotalPrice() {
        return ResponseEntity.ok(
                shoppingCartService.getTotal()
        );
    }
}
