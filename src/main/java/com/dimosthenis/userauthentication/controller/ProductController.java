package com.dimosthenis.userauthentication.controller;

import com.dimosthenis.userauthentication.entity.Product;
import com.dimosthenis.userauthentication.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductServiceImpl productService;

    @GetMapping("/all")
    public ResponseEntity<?> getAllProducts() {
        return ok(productService.getAllProducts());
    }

    @PostMapping("/add-product")
    public ResponseEntity<Product> addProduct(@Valid @RequestBody Product product) {
        return ok(
                productService.addProduct(product)
        );
    }
}
