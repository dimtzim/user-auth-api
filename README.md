POST /api/auth/signup :

 Request 
 body {
    "username": "your-username",
    "email": "your-email:,
    "password": "your-password",
    "role": [] //"user" - for role user, "mod" - fro role moderator, "admin" - for role admin you can write as many role you like
 }
 
 Response
 
 {
    "message": "User registered successfully!"
 }

POST /api/auth/signin

    Request
    
    body = {
        "username": "your username",
        "password": "your password"
    }
    
    Response
    
    id": 2,
    "username": "makis",
    "email": "makis@makis.com",
    "roles": [
        "ROLE_MODERATOR"
    ],
    "accessToken": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtYWtpcyIsInJvbGVzIjpbIlJPTEVfTU9ERVJBVE9SIl0sImlhdCI6MTU4NjEwNjk5MSwiZXhwIjoxNTg2MTEwNTkxfQ.i58BIJ6a6xI73008Cu-PaidOX97GBg4QKcxheJWz1Qk",
    "tokenType": "Bearer"
}
    
    